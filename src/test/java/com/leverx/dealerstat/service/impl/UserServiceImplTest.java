package com.leverx.dealerstat.service.impl;

import com.leverx.dealerstat.model.User;
import com.leverx.dealerstat.model.UserRole;
import com.leverx.dealerstat.repository.UserRepository;
import com.leverx.dealerstat.service.exception.EntryNotFoundException;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {


    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserServiceImpl userService;

    private User defaultUser;


    @BeforeEach
    public void setUp() {
        defaultUser = new User();
        defaultUser.setId(1L);
        defaultUser.setEmail("asd@aswd.asd");
        defaultUser.setFirstName("asd");
        defaultUser.setLastName("asd");
        defaultUser.setPassword("123123");
        defaultUser.setRole(UserRole.TRADER);
    }

    @Test
    void createWithoutError() {
        when(userRepository.save(any(User.class))).thenReturn(defaultUser);
        when(passwordEncoder.encode(anyString())).thenReturn("123123");

        User user = new User();
        user.setEmail("asd@aswd.asd");
        user.setFirstName("asd");
        user.setLastName("asd");
        user.setPassword("123123");
        user.setRole(UserRole.TRADER);

        User save = userService.create(user);

        Assert.assertEquals(user.getEmail(), save.getEmail());
        verify(userRepository, times(1)).save(user);
    }

    @Test
    void findByEmailWithoutError() {
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(defaultUser));

        User byEmail = userService.findByEmail("asd@aswd.asd");

        Assert.assertEquals(defaultUser.getFirstName(), byEmail.getFirstName());
        verify(userRepository, times(1)).findByEmail(anyString());
    }

    @Test
    void findByEmailWillThrowException() {
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.empty());


        Assert.assertThrows(EntryNotFoundException.class, () -> userService.findByEmail("asd@aswd.asd"));
        verify(userRepository, times(1)).findByEmail(anyString());
    }

    @Test
    void findByEmailAndPasswordWithoutError() {
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(defaultUser));
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);

        User user = userService.findByEmailAndPassword("123123", "asd@aswd.asd");

        Assert.assertEquals(defaultUser.getFirstName(), user.getFirstName());
        verify(userRepository, times(1)).findByEmail(anyString());
    }

    @Test
    void findByEmailAndPasswordWillThrowBadCredentialsException() {
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(defaultUser));
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(false);

        Assert.assertThrows(BadCredentialsException.class,
                () -> userService.findByEmailAndPassword("123123", "asd@aswd.asd"));

        verify(userRepository, times(1)).findByEmail(anyString());
    }

    @Test
    void approveById() {
        when(userRepository.save(any(User.class))).thenReturn(defaultUser);
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(defaultUser));

        User user = userService.approveById(1L);

        Assert.assertTrue(user.isApproved());
    }

}