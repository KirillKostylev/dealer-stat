package com.leverx.dealerstat.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.leverx.dealerstat.annotation.UniqueEmail;
import com.leverx.dealerstat.model.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

import static com.leverx.dealerstat.Constants.*;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto {

    private Long id;

    @NotBlank(message = NOT_NULL_OR_EMPTY_MSG)
    @Pattern(regexp = REGEX_FOR_EMAIL, message = EMAIL_PATTERN_MSG)
    @UniqueEmail(message = UNIQUE_EMAIL_MSG)
    private String email;

    @Pattern(regexp = REGEX_FOR_PASSWORD, message = PASSWORD_PATTERN_MSG)
    @Size(min = PASSWORD_MIN_SIZE, max = PASSWORD_MAX_SIZE, message = PASSWORD_SIZE_MSG)
    private String password;

    @NotBlank(message = NOT_NULL_OR_EMPTY_MSG)
    @Pattern(regexp = REGEX_FOR_NAME, message = NAME_PATTERN_MSG)
    @Size(min = NAME_MIN_SIZE, max = NAME_MAX_SIZE, message = NAME_SIZE_MSG)
    private String firstName;

    @NotBlank(message = NOT_NULL_OR_EMPTY_MSG)
    @Pattern(regexp = REGEX_FOR_NAME, message = NAME_PATTERN_MSG)
    @Size(min = NAME_MIN_SIZE, max = NAME_MAX_SIZE, message = NAME_SIZE_MSG)
    private String lastName;

    @Builder.Default
    private UserRole role = UserRole.TRADER;

    private Timestamp createdAt;

    @Builder.Default
    private Double rating = 0.0;

    private boolean approved;
    private boolean isConfirmedEmail;
}
