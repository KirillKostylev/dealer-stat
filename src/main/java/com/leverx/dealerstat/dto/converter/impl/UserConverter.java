package com.leverx.dealerstat.dto.converter.impl;

import com.leverx.dealerstat.dto.UserDto;
import com.leverx.dealerstat.dto.converter.Converter;
import com.leverx.dealerstat.model.User;
import lombok.Builder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Builder
@Component
public class UserConverter implements Converter<User, UserDto> {

    @Override
    public UserDto toDto(User entity) {
        return entity == null ? null : UserDto.builder()
                .id(entity.getId())
                .firstName(entity.getFirstName())
                .lastName(entity.getLastName())
                .email(entity.getEmail())
                .password(entity.getPassword())
                .createdAt(entity.getCreatedAt())
                .rating(entity.getRating())
                .role(entity.getRole())
                .approved(entity.isApproved())
                .isConfirmedEmail(entity.isConfirmedEmail())
                .build();
    }

    @Override
    public User toEntity(UserDto dto) {
        return dto == null ? null : User.builder()
                .id(dto.getId())
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .email(dto.getEmail())
                .password(dto.getPassword())
                .approved(dto.isApproved())
                .rating(dto.getRating())
                .isConfirmedEmail(dto.isConfirmedEmail())
                .createdAt(dto.getCreatedAt())
                .role(dto.getRole())
                .build();
    }

    @Override
    public List<UserDto> toDtoList(List<User> entities) {
        return entities.stream().map(this::toDto).collect(Collectors.toList());
    }
}
