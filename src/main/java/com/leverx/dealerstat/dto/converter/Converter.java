package com.leverx.dealerstat.dto.converter;

import java.util.List;

public interface Converter<E, D> {
    E toEntity(D dto);

    D toDto(E entity);

    List<D> toDtoList(List<E> entities);
}
