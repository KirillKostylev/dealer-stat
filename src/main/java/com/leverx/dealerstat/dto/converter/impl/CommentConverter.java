package com.leverx.dealerstat.dto.converter.impl;

import com.leverx.dealerstat.dto.CommentDto;
import com.leverx.dealerstat.dto.converter.Converter;
import com.leverx.dealerstat.model.Comment;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class CommentConverter implements Converter<Comment, CommentDto> {

    private final UserConverter userConverter;

    @Override
    public CommentDto toDto(Comment entity) {
        return entity == null ? null : CommentDto.builder()
                .id(entity.getId())
                .message(entity.getMessage())
                .rating(entity.getRating())
                .createdAt(entity.getCreatedAt())
                .userDto(null)
//                .userDto(userConverter.toDto(entity.getUser()))
                .approved(entity.isApproved())
                .build();
    }

    @Override
    public Comment toEntity(CommentDto dto) {
        return dto == null ? null : Comment.builder()
                .id(dto.getId())
                .createdAt(dto.getCreatedAt())
                .message(dto.getMessage())
                .rating(dto.getRating())
                .approved(dto.isApproved())
                .user(userConverter.toEntity(dto.getUserDto()))
                .build();
    }

    @Override
    public List<CommentDto> toDtoList(List<Comment> entities) {
        return entities.stream().map(this::toDto).collect(Collectors.toList());
    }

}
