package com.leverx.dealerstat.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.sql.Timestamp;

import static com.leverx.dealerstat.Constants.*;

@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommentDto {
    private Long id;

    @NotBlank(message = NOT_NULL_OR_EMPTY_MSG)
    @Size(min = COMMENT_MSG_MIN_SIZE, max = COMMENT_MSG_MAX_SIZE, message = COMMENT_MSG_SIZE_MESSAGE)
    private String message;

    @NotNull
    @Min(value = RATING_MIN_VALUE, message = RATING_MIN_MSG)
    @Max(value = RATING_MAX_VALUE, message = RATING_MAX_MSG)
    private Integer rating;

    private Timestamp createdAt;

    @Valid
    private UserDto userDto;

    private boolean approved;
}
