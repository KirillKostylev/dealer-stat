package com.leverx.dealerstat.repository;

import com.leverx.dealerstat.model.VerificationToken;
import org.springframework.data.keyvalue.repository.KeyValueRepository;

public interface VerificationTokenRepository extends KeyValueRepository<VerificationToken, String> {
}
