package com.leverx.dealerstat.repository;

import com.leverx.dealerstat.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    String UPDATE_USER_RATING_QUERY = "UPDATE user u, (SELECT avg(c.rating) avg_rating FROM user u JOIN comment c ON u.id = c.user_id WHERE u.id = ?1) r SET u.rating = IFNULL(r.avg_rating,0) WHERE id = ?1";

    Optional<User> findByEmail(String email);

    int deleteUserById(Long id);

    boolean existsByEmail(String email);

    @Modifying
    @Query(value = UPDATE_USER_RATING_QUERY, nativeQuery = true)
    void updateUserRatingByUserId(Long userId);
}
