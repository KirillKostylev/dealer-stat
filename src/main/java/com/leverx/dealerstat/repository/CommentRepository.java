package com.leverx.dealerstat.repository;

import com.leverx.dealerstat.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findAllByUserIdAndApprovedIsTrue(Long userId);

    int deleteCommentById(Long id);

    List<Comment> findAllByApprovedFalse();

}
