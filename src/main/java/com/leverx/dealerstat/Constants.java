package com.leverx.dealerstat;

public class Constants {
    //security
    public static final String AUTHORIZATION = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";

    //validation
    //regex
    public static final String REGEX_FOR_EMAIL = "^[\\w-+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
    public static final String EMAIL_PATTERN_MSG = "must have the email syntax";

    public static final String REGEX_FOR_PASSWORD = "[A-Za-z0-9_]*";
    public static final String PASSWORD_PATTERN_MSG = "must consist of characters A-Z, a-z, 0-9 and '_'";

    public static final String REGEX_FOR_NAME = "[a-zA-ZА-Яа-я]*";
    public static final String NAME_PATTERN_MSG = "must consist of characters A-Z, a-z, А-Я, а-я";

    public static final String REGEX_FOR_TOKEN = "\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12}\\b";
    public static final String TOKEN_PATTERN_MSG = "invalid syntax";

    //size
    public static final int PASSWORD_MIN_SIZE = 4;
    public static final int PASSWORD_MAX_SIZE = 30;
    public static final String PASSWORD_SIZE_MSG = "must be 4 - 30 characters length";

    public static final int NAME_MIN_SIZE = 2;
    public static final int NAME_MAX_SIZE = 30;
    public static final String NAME_SIZE_MSG = "must be 2 - 30 characters length";

    public static final int COMMENT_MSG_MIN_SIZE = 5;
    public static final int COMMENT_MSG_MAX_SIZE = 250;
    public static final String COMMENT_MSG_SIZE_MESSAGE = "must be 5 - 250 characters length";

    //value
    public static final int RATING_MIN_VALUE = 1;
    public static final int RATING_MAX_VALUE = 10;
    public static final String RATING_MIN_MSG = "must be greater than 1";
    public static final String RATING_MAX_MSG = "must be less than 10";

    //other
    public static final String UNIQUE_EMAIL_MSG = "such mail is already registered";
    public static final String NOT_NULL_OR_EMPTY_MSG = "can't be null or empty";


    //exceptions messages
    public static final String USER_WITH_ID_NOT_FOUND_MSG = "User with id = %d not found";
    public static final String USER_WITH_EMAIL_NOT_FOUND_MSG = "User with email = %s not found";
    public static final String COMMENT_WITH_ID_NOT_FOUND_MSG = "Comment with id = %d not found";
    public static final String TOKEN_WITH_ID_NOT_FOUND_MSG = "Token with id = %s not found";
    public static final String TOKEN_HAS_EXPIRED_MSG = "Verification token  = %s has expired";
    public static final String INCORRECT_PASSWORD_MSG = "Incorrect password";
    public static final String DONT_CONFIRMED_EMAIL = "You can't use this method. Confirm your email";

}
