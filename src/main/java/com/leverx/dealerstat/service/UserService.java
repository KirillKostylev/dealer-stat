package com.leverx.dealerstat.service;

import com.leverx.dealerstat.model.User;

import java.util.List;

public interface UserService extends Service<User, Long> {
    User findByEmailAndPassword(String password, String email);

    List<User> findAll();

    List<User> findAllSortingByRating();

    boolean isUserExist(String email);

    User confirmUserEmailByUserId(Long id);

    User findByEmail(String email);

    User changePassword(Long userId, String newPassword);

    User approveById(Long id);
}


