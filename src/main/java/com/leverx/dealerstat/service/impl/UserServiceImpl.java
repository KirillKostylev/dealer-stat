package com.leverx.dealerstat.service.impl;

import com.leverx.dealerstat.model.User;
import com.leverx.dealerstat.repository.UserRepository;
import com.leverx.dealerstat.service.UserService;
import com.leverx.dealerstat.service.exception.EntryNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.leverx.dealerstat.Constants.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService, UserDetailsService {

    private static final String RATING_FIELD = "rating";
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;


//    @Autowired
//    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
//        this.userRepository = userRepository;
//        this.passwordEncoder = passwordEncoder;
//    }

    @Override
    public User findById(Long id) throws EntryNotFoundException {
        return userRepository.findById(id).orElseThrow(() ->
                new EntryNotFoundException(String.format(USER_WITH_ID_NOT_FOUND_MSG, id)));
    }

    @Override
    public User create(User user) {
        if (user.getPassword() != null) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        User newUser = userRepository.save(user);
        log.debug("User = {} was created ", newUser);
        return newUser;
    }

    @Override
    public User update(User user) {
        user = userRepository.save(user);
        log.debug("User = {} was updated ", user);
        return user;
    }

    @Override
    public boolean deleteById(Long id) {
        boolean isDeleted = userRepository.deleteUserById(id) == 1;
        if (!isDeleted) {
            throw new EntryNotFoundException(String.format(USER_WITH_ID_NOT_FOUND_MSG, id));
        }
        log.info("User with id = {} was deleted", id);
        return true;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public boolean isUserExist(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public User confirmUserEmailByUserId(Long id) {
        User user = findById(id);
        user.setConfirmedEmail(true);
        User updatedUser = userRepository.save(user);
        log.debug("User with email = {} confirmed their email", updatedUser.getEmail());
        return updatedUser;
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(() ->
                new EntryNotFoundException(String.format(USER_WITH_EMAIL_NOT_FOUND_MSG, email)));
    }

    @Override
    public User changePassword(Long userId, String newPassword) {
        User user = findById(userId);
        user.setPassword(passwordEncoder.encode(newPassword));
        User updatedUser = update(user);
        log.info("User with id = {} changed password", userId);
        return updatedUser;
    }

    @Override
    public User approveById(Long id) {
        User user = findById(id);
        user.setApproved(true);
        User savedUser = update(user);
        log.debug("Admin approved user with id = {}", user.getId());
        return savedUser;
    }

    @Override
    public User findByEmailAndPassword(String password, String email) {
        User user = findByEmail(email);
        boolean matches = passwordEncoder.matches(password, user.getPassword());
        if (!matches) {
            throw new BadCredentialsException(INCORRECT_PASSWORD_MSG);
        }
        return user;
    }

    @Override
    public List<User> findAllSortingByRating() {
        return userRepository.findAll(Sort.by(Sort.Direction.DESC, RATING_FIELD));
    }


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = findByEmail(s);
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), getAuthority(user));
    }

    private Set<SimpleGrantedAuthority> getAuthority(User user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority(user.getRole().getValue()));
        return authorities;
    }
}
