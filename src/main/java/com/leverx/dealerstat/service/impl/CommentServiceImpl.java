package com.leverx.dealerstat.service.impl;

import com.leverx.dealerstat.model.Comment;
import com.leverx.dealerstat.repository.CommentRepository;
import com.leverx.dealerstat.repository.UserRepository;
import com.leverx.dealerstat.service.CommentService;
import com.leverx.dealerstat.service.exception.EntryNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.leverx.dealerstat.Constants.COMMENT_WITH_ID_NOT_FOUND_MSG;
import static com.leverx.dealerstat.Constants.USER_WITH_ID_NOT_FOUND_MSG;

@Slf4j
@RequiredArgsConstructor
@Service
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final UserRepository userRepository;

    @Override
    public Comment create(Comment comment) {
        Comment newComment = commentRepository.save(comment);
        log.debug("Comment = {} was created", newComment);
        return newComment;
    }

    @Override
    public Comment update(Comment entity) {
        Comment comment = commentRepository.save(entity);
        log.debug("Comment = {} was updated", comment);
        return comment;
    }

    @Override
    public Comment findById(Long commentId) throws EntryNotFoundException {
        return commentRepository.findById(commentId).orElseThrow(() ->
                new EntryNotFoundException(String.format(COMMENT_WITH_ID_NOT_FOUND_MSG, commentId)));
    }

    @Override
    public List<Comment> findApprovedCommentsByUserId(Long userId) throws EntryNotFoundException {
        boolean isUserExists = userRepository.existsById(userId);
        if (!isUserExists) {
            throw new EntryNotFoundException(String.format(USER_WITH_ID_NOT_FOUND_MSG, userId));
        }
        return commentRepository.findAllByUserIdAndApprovedIsTrue(userId);
    }

    @Override
    public List<Comment> findAllDontApproved() {
        return commentRepository.findAllByApprovedFalse();
    }

    @Override
    public Comment approveById(Long id) {
        Comment comment = findById(id);
        comment.setApproved(true);
        Comment savedUser = update(comment);
        userRepository.updateUserRatingByUserId(comment.getUser().getId());
        log.debug("Admin approved the comment with id = {}", comment.getId());
        return savedUser;
    }

    @Override
    public boolean deleteById(Long commentId) {
        boolean isDeleted = commentRepository.deleteCommentById(commentId) == 1;
        if (!isDeleted) {
            throw new EntryNotFoundException(String.format(COMMENT_WITH_ID_NOT_FOUND_MSG, commentId));
        }
        log.info("Comment with id = {} was deleted", commentId);
        return true;
    }
}
