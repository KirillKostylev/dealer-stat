package com.leverx.dealerstat.service.impl;

import com.leverx.dealerstat.model.VerificationToken;
import com.leverx.dealerstat.repository.VerificationTokenRepository;
import com.leverx.dealerstat.service.VerificationTokenService;
import com.leverx.dealerstat.service.exception.EntryNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;

import static com.leverx.dealerstat.Constants.TOKEN_WITH_ID_NOT_FOUND_MSG;

@Slf4j
@RequiredArgsConstructor
@Service
public class VerificationTokenServiceImpl implements VerificationTokenService {

    private final VerificationTokenRepository verificationTokenRepository;

    @Override
    public VerificationToken findById(String key) throws EntryNotFoundException {
        return verificationTokenRepository
                .findById(key)
                .orElseThrow(() -> new EntryNotFoundException(String.format(TOKEN_WITH_ID_NOT_FOUND_MSG, key)));
    }

    @Override
    public boolean deleteById(String key) {
        verificationTokenRepository.deleteById(key);
        boolean isDeleted = !verificationTokenRepository.existsById(key);
        log.debug("Token = {} was deleted", key);
        return isDeleted;
    }

    @Override
    public VerificationToken create(VerificationToken token) {
        VerificationToken save = verificationTokenRepository.save(token);
        log.debug("Saved token = {}", save);
        return save;
    }

    @Override
    public boolean isValid(VerificationToken token) {
        Date currentDate = new Date();
        Date expirationDate = token.getExpirationDate();
        boolean isAfter = expirationDate.before(currentDate);
        log.debug("Token = {} is valid = {}", token, isAfter);
        return isAfter;
    }

    @Override
    public VerificationToken update(VerificationToken entity) {
        return null;
    }
}
