package com.leverx.dealerstat.service.exception;

public class VerificationTokenException extends Exception {
    public VerificationTokenException(String message) {
        super(message);
    }

    public VerificationTokenException(String message, Throwable cause) {
        super(message, cause);
    }
}
