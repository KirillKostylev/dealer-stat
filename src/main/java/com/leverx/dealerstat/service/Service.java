package com.leverx.dealerstat.service;

import com.leverx.dealerstat.service.exception.EntryNotFoundException;
import org.springframework.transaction.annotation.Transactional;

public interface Service<E, K> {
    E findById(K key) throws EntryNotFoundException;

    @Transactional
    boolean deleteById(K key);

    @Transactional
    E create(E entity);

    @Transactional
    E update(E entity);
}
