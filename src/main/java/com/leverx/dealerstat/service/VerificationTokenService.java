package com.leverx.dealerstat.service;

import com.leverx.dealerstat.model.VerificationToken;

public interface VerificationTokenService extends Service<VerificationToken, String> {
    boolean isValid(VerificationToken token);

}
