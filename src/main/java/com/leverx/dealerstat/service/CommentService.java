package com.leverx.dealerstat.service;

import com.leverx.dealerstat.model.Comment;

import java.util.List;

public interface CommentService extends Service<Comment, Long> {
    List<Comment> findApprovedCommentsByUserId(Long userId);

    List<Comment> findAllDontApproved();

    Comment approveById(Long id);
}
