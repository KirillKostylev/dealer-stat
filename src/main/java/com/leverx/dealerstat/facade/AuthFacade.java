package com.leverx.dealerstat.facade;

import com.leverx.dealerstat.dto.*;
import com.leverx.dealerstat.service.exception.VerificationTokenException;
import org.springframework.transaction.annotation.Transactional;

public interface AuthFacade {
    @Transactional
    UserDto confirmUserEmail(String key) throws VerificationTokenException;

    @Transactional
    void sendMessageForPasswordReset(EmailDto emailDto);

    @Transactional
    UserDto resetPassword(ResetPasswordDto resetPasswordDto) throws VerificationTokenException;

    boolean checkKey(String key);

    AuthToken auth(SignInDto signInDto);
}
