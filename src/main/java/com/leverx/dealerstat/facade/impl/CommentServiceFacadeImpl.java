package com.leverx.dealerstat.facade.impl;

import com.leverx.dealerstat.dto.CommentDto;
import com.leverx.dealerstat.dto.converter.impl.CommentConverter;
import com.leverx.dealerstat.facade.CommentServiceFacade;
import com.leverx.dealerstat.model.Comment;
import com.leverx.dealerstat.model.User;
import com.leverx.dealerstat.service.CommentService;
import com.leverx.dealerstat.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class CommentServiceFacadeImpl implements CommentServiceFacade {

    private final CommentService commentService;
    private final UserService userService;
    private final CommentConverter commentConverter;

    @Override
    public CommentDto findById(Long commentId) {
        log.debug("Getting comment by id = {}", commentId);
        Comment comment = commentService.findById(commentId);
        return commentConverter.toDto(comment);
    }

    @Override
    public List<CommentDto> findApprovedCommentsByUserId(Long userId) {
        log.debug("Getting all comments by user with id = {}", userId);
        List<Comment> comments = commentService.findApprovedCommentsByUserId(userId);
        return commentConverter.toDtoList(comments);
    }

    @Override
    public List<CommentDto> findAllDontApproved() {
        log.debug("Finding all don't approved comments");
        List<Comment> comments = commentService.findAllDontApproved();
        return commentConverter.toDtoList(comments);
    }

    @Override
    public CommentDto approveById(Long id) {
        Comment comment = commentService.approveById(id);
        return commentConverter.toDto(comment);
    }

    @Override
    public CommentDto create(Long userId, CommentDto commentDto) {
        log.debug("Creating new Comment for user with id = {}", userId);
        Comment comment = commentConverter.toEntity(commentDto);
        User user = userService.findById(userId);
        comment.setUser(user);
        Comment savedComment = commentService.create(comment);
        return commentConverter.toDto(savedComment);
    }

    @Override
    public CommentDto createCommentAndTrader(CommentDto commentDto) {
        log.debug("Creating new Comment and Trader");
        Comment comment = commentConverter.toEntity(commentDto);
        User user = userService.create(comment.getUser());
        comment.setUser(user);
        Comment savedComment = commentService.create(comment);
        return commentConverter.toDto(savedComment);
    }

    @Override
    public void deleteById(Long id) {
        commentService.deleteById(id);
    }
}
