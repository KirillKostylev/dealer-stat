package com.leverx.dealerstat.facade.impl;

import com.leverx.dealerstat.dto.UserDto;
import com.leverx.dealerstat.dto.converter.impl.UserConverter;
import com.leverx.dealerstat.facade.UserServiceFacade;
import com.leverx.dealerstat.model.User;
import com.leverx.dealerstat.model.VerificationToken;
import com.leverx.dealerstat.service.EmailSenderService;
import com.leverx.dealerstat.service.UserService;
import com.leverx.dealerstat.service.VerificationTokenService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class UserServiceFacadeImpl implements UserServiceFacade {
    private static final String SUBJECT_FOR_REGISTRATION_CONFIRM_MSG = "Registration Confirmation";

    private final UserService userService;
    private final EmailSenderService emailSenderService;
    private final VerificationTokenService verificationTokenService;
    private final UserConverter userConverter;

    @Value("${server.url}" + "${server.port}")
    private String url;


    @Override
    public UserDto create(UserDto userDto) {
        log.debug("Creating new User from UserDto = {}", userDto);
        User user = userConverter.toEntity(userDto);
        User savedUser = userService.create(user);

        VerificationToken token = new VerificationToken(user.getId());
        token = verificationTokenService.create(token);

        String text = String.format("Hello, %s %s. For activation your account go to the link %s/api/auth/confirm/%s",
                user.getFirstName(), user.getLastName(), url, token.getId());

        emailSenderService.send(user.getEmail(), SUBJECT_FOR_REGISTRATION_CONFIRM_MSG, text);
        return userConverter.toDto(savedUser);
    }

    @Override
    public List<UserDto> findAll() {
        log.debug("Getting all users");
        List<User> users = userService.findAll();
        return userConverter.toDtoList(users);
    }

    @Override
    public UserDto findById(Long id) {
        log.debug("Looking for user by id = {}", id);
        User user = userService.findById(id);
        return userConverter.toDto(user);
    }

    @Override
    public void deleteById(Long id) {
        log.debug("Deleting user by id = {}", id);
        userService.deleteById(id);
    }

    @Override
    public List<UserDto> findTopUsers() {
        log.debug("Getting top traders");
        List<User> users = userService.findAllSortingByRating();
        return userConverter.toDtoList(users);
    }

    @Override
    public UserDto approveById(Long id) {
        User user = userService.approveById(id);
        return userConverter.toDto(user);
    }
}
