package com.leverx.dealerstat.facade.impl;

import com.leverx.dealerstat.dto.*;
import com.leverx.dealerstat.dto.converter.impl.UserConverter;
import com.leverx.dealerstat.facade.AuthFacade;
import com.leverx.dealerstat.facade.exception.DontConfirmedEmail;
import com.leverx.dealerstat.jwt.JwtProvider;
import com.leverx.dealerstat.model.User;
import com.leverx.dealerstat.model.VerificationToken;
import com.leverx.dealerstat.service.EmailSenderService;
import com.leverx.dealerstat.service.UserService;
import com.leverx.dealerstat.service.VerificationTokenService;
import com.leverx.dealerstat.service.exception.VerificationTokenException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.leverx.dealerstat.Constants.DONT_CONFIRMED_EMAIL;
import static com.leverx.dealerstat.Constants.TOKEN_HAS_EXPIRED_MSG;

@Slf4j
@Component
@RequiredArgsConstructor
public class AuthFacadeImpl implements AuthFacade {
    private static final int MINUTES_NUMBER_FOR_PASSWORD_RECOVERY = 60;
    private static final String SUBJECT_FOR_RECOVERY_PASSWORD_MSG = "Recovery password";

    private final VerificationTokenService verificationTokenService;
    private final UserService userService;
    private final EmailSenderService emailSenderService;
    private final JwtProvider jwtProvider;
    private final UserConverter userConverter;

    @Override
    public UserDto confirmUserEmail(String tokenId) throws VerificationTokenException {
        VerificationToken token = verificationTokenService.findById(tokenId);
        checkTokenIfInvalidThenThrowException(token);
        User user = userService.confirmUserEmailByUserId(token.getUserId());
        verificationTokenService.deleteById(tokenId);
        return userConverter.toDto(user);
    }

    @Override
    public void sendMessageForPasswordReset(EmailDto emailDto) {
        User user = userService.findByEmail(emailDto.getEmail());
        emailIsConfirmedIfNotThrowException(user.isConfirmedEmail());

        VerificationToken token = new VerificationToken(user.getId(), MINUTES_NUMBER_FOR_PASSWORD_RECOVERY);
        token = verificationTokenService.create(token);

        String text = String.format("Key for password recovery = %s", token.getId());
        emailSenderService.send(emailDto.getEmail(), SUBJECT_FOR_RECOVERY_PASSWORD_MSG, text);
    }

    @Override
    public UserDto resetPassword(ResetPasswordDto resetPasswordDto) throws VerificationTokenException {
        VerificationToken token = verificationTokenService.findById(resetPasswordDto.getKey());
        checkTokenIfInvalidThenThrowException(token);
        User user = userService.changePassword(token.getUserId(), resetPasswordDto.getPassword());
        verificationTokenService.deleteById(resetPasswordDto.getKey());
        return userConverter.toDto(user);
    }

    @Override
    public boolean checkKey(String tokenId) {
        VerificationToken token = verificationTokenService.findById(tokenId);
        return verificationTokenService.isValid(token);
    }

    @Override
    public AuthToken auth(SignInDto signInDto) {
        User user = userService.findByEmailAndPassword(signInDto.getPassword(), signInDto.getEmail());
        emailIsConfirmedIfNotThrowException(user.isConfirmedEmail());
        String token = jwtProvider.generateToken(user.getEmail());
        return new AuthToken(token);
    }

    private void checkTokenIfInvalidThenThrowException(VerificationToken token) throws VerificationTokenException {
        boolean valid = verificationTokenService.isValid(token);
        if (!valid) {
            throw new VerificationTokenException(String.format(TOKEN_HAS_EXPIRED_MSG, token.getId()));
        }
    }

    private void emailIsConfirmedIfNotThrowException(boolean isConfirmedEmail) {
        if (!isConfirmedEmail) {
            throw new DontConfirmedEmail(DONT_CONFIRMED_EMAIL);
        }
    }
}
