package com.leverx.dealerstat.facade;

import com.leverx.dealerstat.dto.CommentDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CommentServiceFacade {
    CommentDto findById(Long id);

    List<CommentDto> findApprovedCommentsByUserId(Long userId);

    List<CommentDto> findAllDontApproved();

    @Transactional
    CommentDto approveById(Long id);

    @Transactional
    CommentDto create(Long userId, CommentDto dto);

    @Transactional
    CommentDto createCommentAndTrader(CommentDto dto);

    @Transactional
    void deleteById(Long id);

}
