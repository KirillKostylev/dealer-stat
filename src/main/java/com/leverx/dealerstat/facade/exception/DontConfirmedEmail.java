package com.leverx.dealerstat.facade.exception;

public class DontConfirmedEmail extends RuntimeException {
    public DontConfirmedEmail(String message) {
        super(message);
    }

    public DontConfirmedEmail(String message, Throwable cause) {
        super(message, cause);
    }
}
