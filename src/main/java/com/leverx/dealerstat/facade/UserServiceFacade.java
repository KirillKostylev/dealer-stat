package com.leverx.dealerstat.facade;

import com.leverx.dealerstat.dto.UserDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserServiceFacade {

    @Transactional
    UserDto create(UserDto userDto);

    List<UserDto> findAll();

    UserDto findById(Long id);

    @Transactional
    void deleteById(Long id);

    List<UserDto> findTopUsers();

    @Transactional
    UserDto approveById(Long id);
}
