package com.leverx.dealerstat.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserRole {
    ADMIN("ROLE_ADMIN"),
    TRADER("ROLE_TRADER");

    private String value;
}
