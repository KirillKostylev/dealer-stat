package com.leverx.dealerstat.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.util.Calendar;
import java.util.Date;

@Data
@NoArgsConstructor
public class VerificationToken {
    public static final int DEFAULT_EXPIRATION_DATE_IN_MIN = 1;

    @Id
    private String id;
    private Long userId;
    private Date expirationDate;

    public VerificationToken(Long userId, int expirationDateInMinutes) {
        this.userId = userId;
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, expirationDateInMinutes);
        expirationDate = calendar.getTime();
    }

    public VerificationToken(Long userId) {
        this(userId, DEFAULT_EXPIRATION_DATE_IN_MIN);
    }
}
