package com.leverx.dealerstat.controller;

import com.leverx.dealerstat.dto.CommentDto;
import com.leverx.dealerstat.facade.CommentServiceFacade;
import com.leverx.dealerstat.service.exception.EntryNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/")
public class CommentController {
    private final CommentServiceFacade commentServiceFacade;

    @GetMapping("/comments/{commentId}")
    public ResponseEntity<CommentDto> findById(@PathVariable Long commentId) throws EntryNotFoundException {
        CommentDto commentDto = commentServiceFacade.findById(commentId);
        return ResponseEntity.ok(commentDto);
    }

    @GetMapping("users/{userId}/comments")
    public ResponseEntity<List<CommentDto>> findApprovedCommentsByUserId(@PathVariable Long userId) {
        List<CommentDto> comments = commentServiceFacade.findApprovedCommentsByUserId(userId);
        return ResponseEntity.ok(comments);
    }

    @PostMapping("users/{userId}/comments")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CommentDto> create(@PathVariable Long userId, @Valid @RequestBody CommentDto commentDto) throws EntryNotFoundException {
        CommentDto createdComment = commentServiceFacade.create(userId, commentDto);
        return ResponseEntity.ok(createdComment);
    }

    @PostMapping("/comments")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CommentDto> createCommentAndTrader(@Valid @RequestBody CommentDto commentDto) throws EntryNotFoundException {
        CommentDto createdComment = commentServiceFacade.createCommentAndTrader(commentDto);
        return ResponseEntity.ok(createdComment);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/comments")
    public ResponseEntity<List<CommentDto>> findAllDontApproved() {
        List<CommentDto> comments = commentServiceFacade.findAllDontApproved();
        return ResponseEntity.ok(comments);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/comments/{id}")
    public ResponseEntity<CommentDto> approve(@PathVariable Long id) {
        CommentDto commentDto = commentServiceFacade.approveById(id);
        return ResponseEntity.ok(commentDto);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/comments/{id}")
    public void delete(@PathVariable Long id) {
        commentServiceFacade.deleteById(id);
    }
}
