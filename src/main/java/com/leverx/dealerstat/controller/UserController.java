package com.leverx.dealerstat.controller;

import com.leverx.dealerstat.dto.UserDto;
import com.leverx.dealerstat.facade.UserServiceFacade;
import com.leverx.dealerstat.service.exception.EntryNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/users")
public class UserController {

    private final UserServiceFacade userServiceFacade;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<UserDto> create(@Valid @RequestBody UserDto userDto) {
        UserDto newUserDto = userServiceFacade.create(userDto);
        return ResponseEntity.ok(newUserDto);
    }

    @GetMapping
    public ResponseEntity<List<UserDto>> findAll() {
        List<UserDto> all = userServiceFacade.findAll();
        return ResponseEntity.ok(all);
    }

    @GetMapping("{id}")
    public ResponseEntity<UserDto> findById(@PathVariable Long id) throws EntryNotFoundException {
        UserDto user = userServiceFacade.findById(id);
        return ResponseEntity.ok(user);
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteById(@PathVariable Long id) {
        userServiceFacade.deleteById(id);
    }

    @GetMapping("top-rating")
    public ResponseEntity<List<UserDto>> findTopUsers() {
        List<UserDto> topUsers = userServiceFacade.findTopUsers();
        return ResponseEntity.ok(topUsers);
    }

    @PutMapping("{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<UserDto> approveUser(@PathVariable Long id) {
        UserDto userDto = userServiceFacade.approveById(id);
        return ResponseEntity.ok(userDto);
    }
}
