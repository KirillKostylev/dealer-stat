package com.leverx.dealerstat.controller;

import com.leverx.dealerstat.dto.*;
import com.leverx.dealerstat.facade.AuthFacade;
import com.leverx.dealerstat.service.exception.VerificationTokenException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/auth/")
public class AuthController {
    private final AuthFacade authFacade;

    @GetMapping("confirm/{key}")
    public ResponseEntity<UserDto> confirmUserEmail(@PathVariable String key) throws VerificationTokenException {
        UserDto userDto = authFacade.confirmUserEmail(key);
        return ResponseEntity.ok(userDto);
    }

    @PostMapping("forgot-password")
    public void sendMessageForPasswordReset(@Valid @RequestBody EmailDto email) {
        authFacade.sendMessageForPasswordReset(email);
    }

    @PostMapping("reset")
    public ResponseEntity<UserDto> passwordReset(@Valid @RequestBody ResetPasswordDto resetPasswordDto) throws VerificationTokenException {
        UserDto userDto = authFacade.resetPassword(resetPasswordDto);
        return ResponseEntity.ok(userDto);
    }

    @GetMapping("check-code/{checkCode}")
    public boolean checkToken(@PathVariable String checkCode) {
        return authFacade.checkKey(checkCode);
    }

    @PostMapping
    public ResponseEntity<AuthToken> auth(@Valid @RequestBody SignInDto signInDto) {
        AuthToken token = authFacade.auth(signInDto);
        return ResponseEntity.ok(token);
    }
}
